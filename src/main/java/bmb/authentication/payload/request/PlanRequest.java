/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmb.authentication.payload.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 *
 * @author hebil
 */
@Data
public class PlanRequest {
     
    @NotBlank
    @Size(max = 20)
    private String name;
    
    @NotBlank
    @Size(max = 20)
    private String limitInscriptions;
    
    private double price;
    
}
