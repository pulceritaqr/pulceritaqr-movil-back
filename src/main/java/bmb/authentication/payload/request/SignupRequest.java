/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bmb.authentication.payload.request;

import bmb.authentication.model.Plan;
import java.util.Set;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @author bmares008
 */
@Data
public class SignupRequest {
    
    
    private Long id;
    
   @NotBlank
  @Size(min = 3, max = 20)
  private String username;

  @NotBlank
  @Size(max = 50)
  @Email
  private String email;

  private Set<String> role;   

  @NotBlank
  @Size(min = 6, max = 40)
  private String password;
  
  
  @Size(max = 50)
  private String address;
  
  @Size(max = 20)
  private String name;
  
  @Size(max = 20)
  private String lastName;
  
  @Size(max = 20)
  private String secondLastName;
  
  private String gender;
   
   
}
