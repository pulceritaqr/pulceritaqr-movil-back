/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmb.authentication.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 *
 * @author hebil
 */
@Entity
@Table(name = "dependent")
@Data
public class Dependent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDependent;
    
    @NotBlank
    @Size(max = 20)
    private String name;
    
    @NotBlank
    private String lastName;
    
    @NotBlank
    private String secondLastName;
    
    @NotBlank
    private String address;
    
    private String image; 
    
    @NotBlank
    private String specialCares;
    
    private String urlQR;
    
    @NotBlank
    private String gender;
    
    private Boolean hasWirst;
    
    private Long idUser;
    
}
