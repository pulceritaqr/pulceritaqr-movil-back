/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bmb.authentication.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 *
 * @author bmares008
 */
@Entity
@Table(name = "user")
@Data
public class User {

    public User() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 20)
    private String name;
    
    @Size(max = 20)
    private String lastName;
    
    @Size(max = 20)
    private String secondLastName;
    
    private String address;
    

    @Size(max = 50)
    @Email
    private String email;

    @Size(max = 120)
    private String password;
    
    @Size(max = 50)
    private String username;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    public User(String name, String lastName, String secondLastName, String email,String password, String gender, String address, String username, Plan plan ) {
         this.name = name;
         this.lastName = lastName;
         this.secondLastName = secondLastName;
         this.email = email;
         this.password = password;
         this.gender = gender;
         this.address = address;
        this.username = username;
        this.plan = plan;
    }
    
 
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="plan_id", referencedColumnName = "idPlan")
    @JsonManagedReference
    private Plan plan;
    
    //@NotBlank
    private String gender;


}
