/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmb.authentication.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;
import org.hibernate.annotations.GeneratorType;

/**
 *
 * @author hebil
 */
@Entity
@Table(name = "plan")
@Data
public class Plan {
    
    public Plan(){
        
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPlan;
    
    @NotBlank
    @Size(max = 20)
    private String name;
    
    @NotBlank
    @Size(max = 20)
    private String limitInscriptions;
    
    private double price;
    
    @OneToOne(mappedBy="plan")
    @JsonBackReference
    private User user;
    
    public Plan(String name, String limitInscriptions, double price){
        this.name = name;
        this.limitInscriptions = limitInscriptions;
        this.price = price;
    }
}
