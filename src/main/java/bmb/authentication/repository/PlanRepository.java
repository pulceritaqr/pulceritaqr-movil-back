/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmb.authentication.repository;

import bmb.authentication.model.Plan;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author hebil
 */
public interface PlanRepository extends JpaRepository<Plan, Long>{
    Optional<Plan> findByName(String name);
}
