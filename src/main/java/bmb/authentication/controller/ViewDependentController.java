/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/Controller.java to edit this template
 */
package bmb.authentication.controller;

import bmb.authentication.model.Dependent;
import bmb.authentication.model.User;
import bmb.authentication.repository.DependentRepository;
import bmb.authentication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author bmares008
 */
@Controller
public class ViewDependentController {

    @Autowired
    DependentRepository dependentRepository;
    
    @Autowired
    UserRepository userRepo;

    @RequestMapping(value = "/dependet-qr/{idDependent}")
    public String page(@PathVariable Long idDependent, Model model) {
        Dependent dep = dependentRepository.findById(idDependent).orElse(null);
        User user = userRepo.findById(dep.getIdUser()).orElse(null);
        model.addAttribute("dep", dep);
        model.addAttribute("user", user);
        return "dependent";
    }

}
