/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmb.authentication.controller;

import bmb.authentication.model.Plan;
import bmb.authentication.payload.request.PlanRequest;
import bmb.authentication.payload.response.MessageResponse;
import bmb.authentication.repository.PlanRepository;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author hebil
 */
@RestController
@RequestMapping("/plan")
public class PlanController {
    
    @Autowired
    private PlanRepository planRepository;
    
    
    @PostMapping("/register")
    public ResponseEntity<?> planRegister(@Valid @RequestBody PlanRequest planRequest){
        Plan plan = new Plan(
                planRequest.getName(),
                planRequest.getLimitInscriptions(),
                planRequest.getPrice()
        );
        
        planRepository.save(plan);
        return ResponseEntity.ok(new MessageResponse("plan registered successfully!"));
    }
    
    @GetMapping("/planes")
    public ResponseEntity<?> listaPlanes(){
       List<Plan> planes =  planRepository.findAll();
       
       return ResponseEntity.ok(planes);
    }
    
}
