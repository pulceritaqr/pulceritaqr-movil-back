/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/RestController.java to edit this template
 */
package bmb.authentication.controller;

import bmb.authentication.model.Dependent;
import bmb.authentication.repository.DependentRepository;
import bmb.authentication.response.ResponseHandler;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author bmares008
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class DependentController {

    @Autowired
    DependentRepository dependentRepository;
    

    @GetMapping("/my-dependents/{idUser}")
    public ResponseEntity<?> getDependentOfUser(@PathVariable Long idUser) {
        try {

            List<Dependent> dependientesOfU = dependentRepository.findByIdUser(idUser);

            return ResponseHandler.generateResponse("Lista mis dependientes", 
                    HttpStatus.OK, dependientesOfU,false);
        } catch (Exception e) {
            return ResponseHandler.generateResponse("Error", 
                    HttpStatus.MULTI_STATUS, e, true);
        }
    }
    
    
    @PostMapping("/add-dependent/{idUser}")
    public ResponseEntity<?> addDependentOfUser(@PathVariable Long idUser,
            @RequestBody Dependent reqDependt) {
        try{
            reqDependt.setIdUser(idUser);
            Dependent dependtSave = dependentRepository.save(reqDependt);
            
            dependtSave.setUrlQR("/dependet-qr/"+dependtSave.getIdDependent());
            dependentRepository.save(dependtSave);
            return ResponseHandler.generateResponse("Dependiente guardado correctamente", 
                    HttpStatus.OK, dependtSave, false);
              
        }catch(Exception e){
            return ResponseHandler.generateResponse("Error", 
                    HttpStatus.MULTI_STATUS, e, true);
        }
    }
    
    @DeleteMapping("/delete-dependent/{idDependent}")
    public ResponseEntity<?> deleteDependent(@PathVariable Long idDependent) {
        try{
            if(dependentRepository.findById(idDependent).orElse(null) == null){
                return ResponseHandler.generateResponse("Error este dependiente no existe", 
                    HttpStatus.NOT_FOUND, null, false);
            }
            
            dependentRepository.deleteById(idDependent);
            
            return ResponseHandler.generateResponse("Dependiente eliminado correctamente", 
                    HttpStatus.OK, null, false);
              
        }catch(Exception e){
            return ResponseHandler.generateResponse("Error", 
                    HttpStatus.MULTI_STATUS, e, true);
        }
    }
    
    
    @PutMapping("/put-dependent")
    public ResponseEntity<?> updateDependent(@RequestBody Dependent reqDependt) {
        try{
            if(dependentRepository.findById(reqDependt.getIdDependent()).orElse(null) == null){
                return ResponseHandler.generateResponse("Error este dependiente no existe", 
                    HttpStatus.NOT_FOUND, null, false);
            }
            
            
            dependentRepository.save(reqDependt);
            return ResponseHandler.generateResponse("Dependiente actualizado correctamente", 
                    HttpStatus.OK, reqDependt, false);
              
        }catch(Exception e){
            return ResponseHandler.generateResponse("Error", 
                    HttpStatus.MULTI_STATUS, e, true);
        }
        
        
    }
    
    
}
