/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bmb.authentication.controller;

import bmb.authentication.model.ERole;
import bmb.authentication.model.Plan;
import bmb.authentication.model.Role;
import bmb.authentication.model.User;
import bmb.authentication.payload.request.LoginRequest;
import bmb.authentication.payload.request.SignupRequest;
import bmb.authentication.payload.response.JwtResponse;
import bmb.authentication.payload.response.MessageResponse;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bmb.authentication.repository.RoleRepository;
import bmb.authentication.repository.UserRepository;
import bmb.authentication.security.jwt.JwtUtils;
import bmb.authentication.security.service.UserDetailsImpl;
import java.util.Optional;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author bmares008
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserRepository userRepository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  PasswordEncoder encoder;

  @Autowired
  JwtUtils jwtUtils;

  //Login
  @PostMapping("/login")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
      
    Optional<User> user = userRepository.findByUsername(loginRequest.getUsername());
    if(user.isEmpty()){
        return ResponseEntity.ok(new MessageResponse("Username not found, please register"));
    }
        

    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);
    
    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();    
    List<String> roles = userDetails.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());

    return ResponseEntity.ok(new JwtResponse(jwt, 
                         userDetails.getId(), 
                         userDetails.getUsername(), 
                         userDetails.getEmail(), 
                         roles));
  }

  //Registro
  @PostMapping("/register")
  public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
    if (userRepository.existsByUsername(signUpRequest.getUsername())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Username exist"));
    }

    if (userRepository.existsByEmail(signUpRequest.getEmail())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Email exist"));
    }

    Plan plan = new Plan("Plan gratis","3",0);
    // Create new user's account
    User user = new User(signUpRequest.getName(),
            signUpRequest.getLastName(),
            signUpRequest.getSecondLastName(),
            signUpRequest.getEmail(),
            encoder.encode(signUpRequest.getPassword()),
            signUpRequest.getGender(),
            signUpRequest.getAddress(),
               signUpRequest.getUsername(), 
               plan);

    Set<String> strRoles = signUpRequest.getRole();
    Set<Role> roles = new HashSet<>();

    if (strRoles == null) {
      Role userRole = roleRepository.findByName(ERole.ROLE_USER)
          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
      roles.add(userRole);
    } else {
      strRoles.forEach(role -> {
        switch (role) {
        case "admin":
          Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
          roles.add(adminRole);

          break;
        default:
          Role userRole = roleRepository.findByName(ERole.ROLE_USER)
              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
          roles.add(userRole);
        }
      });
    }

    user.setRoles(roles);
    userRepository.save(user);

    return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
  }
  
  //Editar usuario
  @PutMapping("/edit")
  public ResponseEntity<?> editarUser(@Valid @RequestBody User user){
      
      Optional<User> userSave = userRepository.findById(user.getId());
      if(userSave == null){
          return ResponseEntity.badRequest().body(new MessageResponse("No se encontro el usuario"));
      } 
     
      
      userRepository.save(user);
      
      return ResponseEntity.ok(new MessageResponse("User update successfully!"));
  }
  
  //Consultar usuario por id
  @GetMapping("/user/{id}")
   public ResponseEntity<?> user(@PathVariable Long id){
       
       Optional<User> userFound = userRepository.findById(id);
       if(userFound == null){
           return ResponseEntity.badRequest().body(new MessageResponse("No se encontro el usuario"));
       }
       
       return ResponseEntity.ok(userFound);
   }
}
